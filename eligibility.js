console.log(mdc);

const MDCTextField = mdc.textField.MDCTextField;
const textFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
  document.querySelectorAll(".mdc-checkbox"),
  function (el) {
    return new MDCCheckbox(el);
  }
);

//variables
const ageField = document.forms[0][0];
const birthField = document.forms[0][1];
const nameField = document.forms[0][2];
const usernameField = document.forms[0][3];
const passwordField = document.forms[0][4];
const confirmField = document.forms[0][5];
const legalCBX = document.forms[0][6];
const termsCBX = document.forms[0][7];
const joinBtn = document.forms[0][8];

// const currentDate = new Date();
// const minimumDate = new Date(currentDate - )

const fullJoinForm = [
  ageField,
  birthField,
  nameField,
  usernameField, 
  passwordField, 
  confirmField,
  legalCBX,
  termsCBX,
  joinBtn,
];

//---- VALIDATION METHODS ----
//Empty Fields
function isNotEmpty(field){
  return field !== '';
}
function noEmptyFields(allFields){
  return allFields.every(isNotEmpty);
}
//Password Match
function passMatch(){
  return passwordField.value === confirmField.value;
}

function validAge(age){
  return age >= 13;
}

//---- VALIDATE FULL FORM ----
function validEligibility(){
  event.preventDefault();
  let section = ["Age", "Birth Date", "Full Name", "Username", "Enter Password", "Confirm Password"];
  const updateValues = [];

  //display values:
  for(i = 0; i < section.length; i++){
    console.log(`${section[i]}: ${fullJoinForm[i].value}`);
    updateValues.push(fullJoinForm[i].value); //
  }

  //checkbox validation
  let boxes = {"legal": legalCBX.checked, "terms": termsCBX.checked};
  let boxValues= legalCBX.checked && termsCBX.checked;
  if(boxes.legal){
    console.log("The user has checked the legal checkbox");
  } else {
    console.log("The user has not checked the legal checkbox");
  }
  if(boxes.terms){
    console.log("The user has checked the terms checkbox");
  } else {
    console.log("The user has not checked the terms checkbox");
  }

  //password validation
  const doMatch = passMatch(); //boolean
  const legalAge = validAge(updateValues[0]);

  //final verdict
  if (noEmptyFields(updateValues) && boxValues && legalAge && passMatch()){
    console.log("The user is eligible");
  }else{
    console.log("The user is ineligible");
  }
 
}

joinBtn.addEventListener("click", validEligibility);



  // for (const key in boxes) {
  //   if(boxes.key){
  //     console.log(`The user has checked the ${boxes.key} checkbox`);
  //   } else {
  //     console.log(boxes.key);
  //     console.log(`The user has not checked the ${boxes.key} checkbox`); 
  //   }
  // }